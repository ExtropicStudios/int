<?php

# sends an email if you know the password

require_once './password.php';

# a message must be posted
if (empty($_POST['message'])) {
	echo "No message";
	http_response_code(400);
	exit();
}

if ($_POST['password'] != $password) {
	echo "Wrong password";
	http_response_code(401);
	exit();
}

$username = $_POST['username'] ?? 'anonymous';

mail('webmaster@extropicstudios.com', 'message from ' . $username, $_POST['message']);
echo "Mail sent";
